package net.iesmila.carcassoneapp.model;

import java.util.ArrayList;

/**
 * Created by BERNAT on 13/04/2016.
 */
public class Tarja {    private static final int NUM_ROTACIONS = 4;
    //-------------------------------------------------------
    private DefinicioTarja mDefinicio;
    private int mRotacio = 0;
    private static ArrayList<Tarja> mTarges;

    public static ArrayList<Tarja> getTarges() {
        if(mTarges ==null) {
            mTarges = new ArrayList<Tarja>();
            for(DefinicioTarja def: DefinicioTarja.getDefinicionsCartes()){
                for(int i=0;i<def.getNumCopies();i++) {
                    mTarges.add(new Tarja(def));
                }
            }
        }
        return mTarges;
    }

    public Tarja(DefinicioTarja mDefinicio) {
        this.mDefinicio = mDefinicio;
        mRotacio = 0;
    }

    public DefinicioTarja getDefinicio() {
        return mDefinicio;
    }

    public int getRotacio() {
        return mRotacio;
    }
    public void giraCW() {
        mRotacio = (mRotacio+1)%NUM_ROTACIONS;
    }
    // Donada una carta,  ens dona el que hi ha a cada lateral
    // (adalt, dreta, avall, esquerra) tenint en compte
    // com està girada.
    public TipusLateral[] getLaterals(){
        TipusLateral laterals[] = mDefinicio.getLaterals();
        TipusLateral lateralsGirats[] = new TipusLateral[NUM_ROTACIONS];
        int idx;
        for( int i=0;i<NUM_ROTACIONS;i++ ){
            idx = i - mRotacio;
            if(idx<0) idx+=NUM_ROTACIONS;
            lateralsGirats[i] = laterals[idx];
        }
        return lateralsGirats;
    }
}