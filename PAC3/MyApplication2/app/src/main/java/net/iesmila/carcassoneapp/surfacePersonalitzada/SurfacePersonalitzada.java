package net.iesmila.carcassoneapp.surfacePersonalitzada;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import net.iesmila.carcassoneapp.MainActivity;
import net.iesmila.carcassoneapp.model.DefinicioTarja;
import net.iesmila.carcassoneapp.model.Tarja;
import net.iesmila.carcassoneapp.model.Tauler;

/**
 * Created by Marcc on 26/05/2016.
 */
public class SurfacePersonalitzada extends SurfaceView implements SurfaceHolder.Callback{
    private FilSurface mFilSurface;
    private boolean mIniciJoc;
    private MainActivity mActivity;
    private Tauler mTaulerJoc;
    private Tarja mTarjaAnterior;
    private Bitmap mTaulerBuffer;
    private Canvas mCanvasTauler;
    private boolean posicioValida;
    private Coord mPantallaTauler = new Coord(17*80,17*80);
    private Coord mUltimToc = new Coord(20*80,20*80);
    private Coord posFinalDelMoviment = new Coord(17*80,17*80);
    private boolean cartaPintada = false;
    private Coord mPosicioVisible = new Coord(20*80,20*80);

    public SurfacePersonalitzada(Context context){
        this(context, null);
    }

    public SurfacePersonalitzada(Context context, AttributeSet attrs) {
        super(context, attrs);
        mActivity = (MainActivity) context;
        getHolder().addCallback(this);

        //Posem com a tarja anterior la inicial
        mTarjaAnterior = new Tarja(DefinicioTarja.getTarjaInicial());
    }

    @Override
    public void draw(Canvas canvas) {
        if(canvas == null) return;
        super.draw(canvas);
        agafemXYTauler();
        if(mTaulerJoc.getNumTargesPendents() != 0) {
            if (mIniciJoc) { //Dibuixar la primera carta
                dibuixarCartaAlCanvasDeJoc(
                        mCanvasTauler,
                        mUltimToc.getX(),
                        mUltimToc.getY(),
                        new Paint(Paint.FILTER_BITMAP_FLAG),
                        new Tarja(DefinicioTarja.getTarjaInicial())
                );
                mIniciJoc = false;
            }
            //Aqui, la carta en teoria ja està canviada
            if (mTarjaAnterior != mTaulerJoc.getTarjaActual()) {
                dibuixarCartaAlCanvasDeJoc(mCanvasTauler,mUltimToc.getX(),mUltimToc.getY(),new Paint(Paint.FILTER_BITMAP_FLAG),mTarjaAnterior);
                cartaPintada = false;
                posicioValida = false;
            }

            Rect desti = new Rect(0, 0, getWidth(), getHeight());
            Rect origen = new Rect(mPantallaTauler.getX(), mPantallaTauler.getY(), getWidth() + mPantallaTauler.getX(), getHeight() + mPantallaTauler.getY());
            canvas.drawBitmap(mTaulerBuffer, origen, desti, new Paint(Paint.FILTER_BITMAP_FLAG));

            if (cartaPintada) {
                dibuixarCartaColocada(canvas);
            }
            estatBtnOK();
        }else{
            Rect desti = new Rect(0, 0, getWidth(), getHeight());
            Rect origen = new Rect(mPantallaTauler.getX(), mPantallaTauler.getY(), getWidth() + mPantallaTauler.getX(), getHeight() + mPantallaTauler.getY());
            canvas.drawBitmap(mTaulerBuffer, origen, desti, new Paint(Paint.FILTER_BITMAP_FLAG));
        }
    }

    private void agafemXYTauler() {
        if(mPantallaTauler.getX() < 0)
        {
            mPantallaTauler.setX(0);
        }
        if(mPantallaTauler.getX() > 3200)
        {
            mPantallaTauler.setX(3200);
        }
        if(mPantallaTauler.getY() < 0)
        {
            mPantallaTauler.setY(0);
        }
        if(mPantallaTauler.getY() > 3200)
        {
            mPantallaTauler.setY(3200);
        }
    }

    private void dibuixarCartaColocada(Canvas canvas) {

        Coord aux = new Coord(mPosicioVisible.getX(), mPosicioVisible.getY());
        int fil = mUltimToc.getY() / 80;
        int collins = mUltimToc.getX() / 80;
        dibuixarCartaAlCanvasDeJoc(canvas,aux.getX(),aux.getY(),new Paint(Paint.FILTER_BITMAP_FLAG),mTaulerJoc.getTarjaActual());
        Paint p = new Paint();
        posicioValida = mTaulerJoc.colocaTarjaActual(fil,collins);
        if(!posicioValida){
            //Si no es pot colocar bé la carta la pintem de vermell--A full HD PAINT IT BLOOOOOOOOOD SATANIC POWAH 666
            p.setARGB(50,255,0,0);
            canvas.drawRect(aux.getX(),aux.getY(),aux.getX() + 80,aux.getY() + 80,p);
        }
    }

    private void dibuixarCartaAlCanvasDeJoc(Canvas canvas, int posX, int posY, Paint paint, Tarja tarja) {
        if(tarja == null) return;
        Bitmap imatge = BitmapFactory.decodeResource(mActivity.getResources(),
                tarja.getDefinicio().getImatge());

        int rotacio=0;
        switch(tarja.getRotacio()){
            case 0:
                rotacio = 360;
                break;
            case 1:
                rotacio = 90;
                break;
            case 2:
                rotacio = 180;
                break;
            case 3:
                rotacio = 270;
                break;
            default:
                return;
        }
        //Matrix necessari per poder rotar el BitMap a la par que s'ha rotat la tarja
        Matrix matrix = new Matrix();
        matrix.postRotate(rotacio);

        Bitmap rotatedBitmap = Bitmap.createBitmap(imatge , 0, 0, imatge.getWidth(), imatge.getHeight(), matrix, true);

        Rect desti = new Rect(posX,posY,posX + 80, posY + 80);
        Rect origen = new Rect(0, 0, imatge.getWidth(),imatge.getHeight());

        canvas.drawBitmap(rotatedBitmap,origen,desti,paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        int eventAction = event.getAction();
        //Centrem on toquem
        float posX = event.getX() - 80;
        float posY = event.getY() - 80;
        mTarjaAnterior = mTaulerJoc.getTarjaActual();

        switch (eventAction){
            case MotionEvent.ACTION_DOWN:
                Log.e("ENTREM AL ACTION DOWN",""+ mUltimToc);
                mPosicioVisible = new Coord(corretgirErrorsCoordenades((int) posX), corretgirErrorsCoordenades((int) posY));
                mUltimToc = new Coord(corretgirErrorsCoordenades((int) (posX + mPantallaTauler.getX())), corretgirErrorsCoordenades((int) (posY + mPantallaTauler.getY())));
                Log.e("SORTIM DEL ACTION DOWN",""+ mUltimToc);
                cartaPintada = true;
                break;
            case MotionEvent.ACTION_MOVE:
                Log.e("ENTREM AL ACTION MOVE", mPantallaTauler +"");
                cartaPintada = false;
                posFinalDelMoviment = new Coord(corretgirErrorsCoordenades((int) (posX + mPantallaTauler.getX())), corretgirErrorsCoordenades((int) (posY + mPantallaTauler.getY())));
                int restaX = posFinalDelMoviment.getX() - mUltimToc.getX();
                int restaY = posFinalDelMoviment.getY() - mUltimToc.getY();
                mPantallaTauler.setX(mPantallaTauler.getX() - restaX);
                mPantallaTauler.setY(mPantallaTauler.getY() - restaY);
                Log.e("SORTIM DEL MOVE", mPantallaTauler + "");
                break;
            case MotionEvent.ACTION_UP:
                break;
        }
        estatBtnOK();
        return true;
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mTaulerBuffer = Bitmap.createBitmap(3200,3200, Bitmap.Config.ARGB_8888);
        mCanvasTauler = new Canvas(mTaulerBuffer);
        mFilSurface = new FilSurface(this.getHolder(),this);
        mFilSurface.start();
        mTaulerJoc = mActivity.getTauler();
        mIniciJoc = true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(mFilSurface !=null){
            mFilSurface.setEnExecucio(false);
            while(true){
                try{
                    mFilSurface.join();
                    break;
                }catch (InterruptedException ex){}
            }
        }
    }
    private int corretgirErrorsCoordenades(int i) {
        int pos = i / 80; //Dividim pel que fa la peca
        pos = pos * 80; //Multipliquem pel que fa la peca
        return pos;
    }

    private void estatBtnOK() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mActivity.setVisibleBotoOk(posicioValida);
            }
        });
    }
}