package net.iesmila.carcassoneapp.surfacePersonalitzada;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Marcc on 26/05/2016.
 */
public class FilSurface extends Thread{
    private SurfaceHolder holder;
    private SurfacePersonalitzada surfacePersonalitzada;
    private boolean enExecucio;

    public FilSurface(SurfaceHolder pholder, SurfacePersonalitzada psurface){
        this.holder = pholder;
        this.surfacePersonalitzada = psurface;
        enExecucio = false;
    }

    public void setEnExecucio(boolean exec){
        this.enExecucio = exec;
    }

    @Override
    public void run() {
        enExecucio = true;
        Canvas canvas;
        while(enExecucio){
            canvas = null;
            try{
                canvas = holder.lockCanvas();
                synchronized (holder){
                    surfacePersonalitzada.draw(canvas);
                }
            }finally {
                if(canvas != null){
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}

