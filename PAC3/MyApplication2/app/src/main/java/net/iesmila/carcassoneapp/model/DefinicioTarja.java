package net.iesmila.carcassoneapp.model;

import android.support.annotation.DrawableRes;

import net.iesmila.carcassoneapp.R;

import java.util.ArrayList;

import static net.iesmila.carcassoneapp.model.TipusLateral.*;

/**
 * Created by BERNAT on 13/04/2016.
 */
public class DefinicioTarja {

    public static ArrayList<DefinicioTarja> mDefinicions;

    public static DefinicioTarja getTarjaInicial(){
        return new DefinicioTarja(R.drawable.card25, 1, CASTELL, CAMI, TERRA, CAMI);
    }
    public static ArrayList<DefinicioTarja> getDefinicionsCartes() {
        if(mDefinicions==null) {
            mDefinicions = new ArrayList<DefinicioTarja>();
            mDefinicions.add(new DefinicioTarja(R.drawable.card1, 4, TERRA, TERRA, TERRA, TERRA));
            mDefinicions.add(new DefinicioTarja(R.drawable.card2, 2, TERRA, TERRA, CAMI, TERRA));
            mDefinicions.add(new DefinicioTarja(R.drawable.card3, 1, CASTELL, CASTELL, CASTELL, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card4, 3, CASTELL, CASTELL, TERRA, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card5, 1, CASTELL, CASTELL, TERRA, CASTELL));
            //-------------------------------------------------------------------------

            mDefinicions.add(new DefinicioTarja(R.drawable.card6, 1, CASTELL, CASTELL, CAMI, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card7, 2, CASTELL, CASTELL, CAMI, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card8, 3, CASTELL, TERRA, TERRA, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card9, 2, CASTELL, TERRA, TERRA, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card10, 3, CASTELL, CAMI, CAMI, CASTELL));
            //-------------------------------------------------------------------------

            mDefinicions.add(new DefinicioTarja(R.drawable.card11, 2, CASTELL, CAMI, CAMI, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card12, 1, TERRA, CASTELL, TERRA, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card13, 2, TERRA, CASTELL, TERRA, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card14, 2, CASTELL, TERRA, TERRA, CASTELL));
            mDefinicions.add(new DefinicioTarja(R.drawable.card15, 3, CASTELL, TERRA, CASTELL, TERRA));
            //-------------------------------------------------------------------------
            mDefinicions.add(new DefinicioTarja(R.drawable.card16, 5, CASTELL, TERRA, TERRA, TERRA));
            mDefinicions.add(new DefinicioTarja(R.drawable.card17, 3, CASTELL, TERRA, CAMI, CAMI));
            mDefinicions.add(new DefinicioTarja(R.drawable.card18, 3, CASTELL, CAMI, CAMI, TERRA));
            mDefinicions.add(new DefinicioTarja(R.drawable.card19, 3, CASTELL, CAMI, CAMI, CAMI));
            mDefinicions.add(new DefinicioTarja(R.drawable.card20, 3, CASTELL, CAMI, TERRA, CAMI));
            //-------------------------------------------------------------------------
            mDefinicions.add(new DefinicioTarja(R.drawable.card21, 8, CAMI, TERRA, CAMI, TERRA));
            mDefinicions.add(new DefinicioTarja(R.drawable.card22, 9, TERRA, TERRA, CAMI, CAMI));
            mDefinicions.add(new DefinicioTarja(R.drawable.card23, 4, TERRA, CAMI, CAMI, CAMI));
            mDefinicions.add(new DefinicioTarja(R.drawable.card24, 1, CAMI, CAMI, CAMI, CAMI));

        }
        return mDefinicions;
    }

    private @DrawableRes int mImatge;
    private TipusLateral mLaterals[]= new TipusLateral[4];
    private int mCopies;

    public DefinicioTarja(@DrawableRes int mImatge,
                          int mCopies,
                          TipusLateral top,
                          TipusLateral right,
                          TipusLateral bottom,
                          TipusLateral left) {
        this.mImatge = mImatge;
        this.mLaterals[0] = top;
        this.mLaterals[1] = right;
        this.mLaterals[2] = bottom;
        this.mLaterals[3] = left;
        this.mCopies = mCopies;
    }

    public int getImatge() {
        return mImatge;
    }

    public TipusLateral[] getLaterals() {
        return mLaterals;
    }

    public int getNumCopies() {
        return mCopies;
    }
}
