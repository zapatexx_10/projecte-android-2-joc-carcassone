package net.iesmila.carcassoneapp;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import net.iesmila.carcassoneapp.model.Tauler;


public class MainActivity extends ActionBarActivity {

    private Tauler tauler;
    private ImageView btnBotoOK;
    private ImageView imvBaralla;
    private net.iesmila.carcassoneapp.surfacePersonalitzada.SurfacePersonalitzada surfacePersonalitzada;
    private ImageView tarjaActual;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnBotoOK = (ImageView) findViewById(R.id.btnOK);
        imvBaralla = (ImageView) findViewById(R.id.imvPilaCartes);
        tarjaActual = (ImageView) findViewById(R.id.imvTarjaActual);
        surfacePersonalitzada = (net.iesmila.carcassoneapp.surfacePersonalitzada.SurfacePersonalitzada) findViewById(R.id.surfaceTauler);

        tauler= new Tauler();

        //Posem el botó invisible fins que la tarja tingui un costat en OK
        setVisibleBotoOk(false);
        //Agaem laprimera carta
        tarjaActual.setImageResource(tauler.getTarjaActual().getDefinicio().getImatge());

        //SET ONCLICKLISTENERS ---------------------------------------------------------------------

        tarjaActual.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                if(tauler.getTarjaActual() == null) return;
                tauler.getTarjaActual().giraCW();
                int gir=0;
                int girAnterior=0;
                switch(tauler.getTarjaActual().getRotacio()){
                    case 0:
                        girAnterior = 270;
                        gir = 360;
                        break;
                    case 1:
                        girAnterior = 0;
                        gir = 90;
                        break;
                    case 2:
                        girAnterior = 90;
                        gir = 180;
                        break;
                    case 3:
                        girAnterior = 180;
                        gir = 270;
                        break;
                    default:
                        return;
                }
                //girem la carta segons la rotacio qe pertoca jajaja no soy 100tifico
                tarjaActual.setRotation((float)girAnterior);
                tarjaActual.animate().rotation(gir).withLayer().start();
            }
        });
        btnBotoOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tauler.getTarjaActual()==null){
                    //Si no queden mes cartes guanyem
                    tarjaActual.setImageResource(R.drawable.youwin);
                }
                tauler.confirmaTarjaActual();
                if (tauler.getNumTargesPendents()==0){
                    tarjaActual.setRotation(0);
                    tarjaActual.setImageResource(R.drawable.youwin);
                }else{
                    tarjaActual.setImageResource(tauler.getTarjaActual().getDefinicio().getImatge());
                    nextCarta();
                    
                }
            }
        });
    }

    private void nextCarta() {
        int rotacio= 0;
        if (tauler.getTarjaActual()==null){
            return;
        }
        switch (tauler.getTarjaActual().getRotacio()){
            case 0:
                rotacio=360;
                break;
            case 1:
                rotacio=90;
                break;
            case 2:
                rotacio=180;
                break;
            case 3:
                rotacio=270;
                break;
            default:
                return;
        }
        tarjaActual.setRotation(rotacio);

        femAnimacioDeLaCarta(tarjaActual);

    }

    private void femAnimacioDeLaCarta(ImageView tarjaActual) {
        TranslateAnimation treureCarta = new TranslateAnimation(0.0f,0.0f, imvBaralla.getWidth(), 0.0f);
        treureCarta.setDuration(1000);
        treureCarta.setRepeatCount(0);
        treureCarta.setRepeatMode(0);
        treureCarta.setFillAfter(true);
        treureCarta.setInterpolator(new LinearInterpolator());
        tarjaActual.startAnimation(treureCarta);
        tarjaActual.setRotationY(0);
        tarjaActual.animate().setDuration(1000).rotationY(360);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setVisibleBotoOk(boolean visibleBotoOk) {
        if (visibleBotoOk ==true){
            btnBotoOK.setVisibility(View.VISIBLE);
        }else{
            btnBotoOK.setVisibility(View.INVISIBLE);
        }
    }
    public Tauler getTauler() {
        return tauler;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        nextCarta();
    }
}
