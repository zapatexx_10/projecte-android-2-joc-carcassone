package net.iesmila.carcassoneapp.model;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by BERNAT on 13/04/2016.
 */
public class Tauler {

    private static final int FILES = 40;
    private static final int COLUMNES = 40;
    private Tarja mTauler[][];
    private ArrayList<Tarja> mTargesPendents;
    private int mPosicioTarjaActualX,mPosicioTarjaActualY;
    private Tarja mTarjaActual;
    private boolean mTarjaActualColocada;

    public Tauler() {
        mTauler = new Tarja[FILES][COLUMNES];
        for(int i=0; i < FILES; i++){
            for(int x=0; x < COLUMNES; x++){
                mTauler[i][x] = null;
            }
        }
        mTauler[20][20] = new Tarja(DefinicioTarja.getTarjaInicial());
        mTargesPendents = Tarja.getTarges();
        Collections.shuffle(mTargesPendents);
        mTarjaActual = mTargesPendents.get(0);
        mTarjaActualColocada = false;
    }
    public Tarja getTarja(int fil, int col) {
        return mTauler[fil][col];
    }

    /**
     *  @return la tarja actual o null si no n'hi ha més !!
     */
    public Tarja getTarjaActual() {

        return mTarjaActual;
    }

    public int getPosicioTarjaActualX() {
        return mPosicioTarjaActualX;
    }

    public int getPosicioTarjaActualY() {
        return mPosicioTarjaActualY;
    }

    public void girarTarjaActual() {
        if(mTarjaActual!=null) {
            mTarjaActual.giraCW();
        }
    }
    public boolean colocaTarjaActual(int fil, int col) {
        if(fil < 0 || col < 0) return false;
        if(fil >= 40 || col >= 40) return false;
        boolean esPot = checkPosicioFilCol(fil, col);
        if(esPot) {
            mPosicioTarjaActualX = col;
            mPosicioTarjaActualY = fil;
            mTarjaActualColocada = true;
            return true;
        } else {
            return false;
        }
    }

    public boolean confirmaTarjaActual(){
        boolean ok = false;
        if(mTarjaActual!=null && mTarjaActualColocada) {
            mTargesPendents.remove(0);
            if(mTargesPendents.size()>0) {
                mTauler[mPosicioTarjaActualY][mPosicioTarjaActualX] = mTarjaActual;
                mTarjaActual = mTargesPendents.get(0);
                ok = true;
            } else {
                mTarjaActual = null;
                mTarjaActualColocada = false;
            }
            mTarjaActualColocada = false;
        }
        return ok;
    }

    public int getNumTargesPendents() {
        return mTargesPendents.size();
    }

    private boolean checkPosicioFilCol(int fil, int col) {
        Tarja tarjaActual = getTarjaActual();
        if (tarjaActual ==null){
            return false;
        }
        TipusLateral[] lateralsActuals = tarjaActual.getLaterals();
        int phil,collins;
        int col_veina_start,col_veina_end;
        int fil_veina_start, fil_veina_end;

        //Comprovem les files i assignem el valor corresponent
        if (col==0){
            col_veina_start=col;
            col_veina_end = col+1;
        }else if (col==COLUMNES-1){
            col_veina_start=col-1;
            col_veina_end=col;
        }else{
            col_veina_start=col-1;
            col_veina_end=col+1;
        }
        //Ara amb les files .....
        if (fil==0){
            fil_veina_start=fil;
            fil_veina_end = fil+1;
        }else if (fil==FILES-1){
            fil_veina_start=fil-1;
            fil_veina_end=fil;
        }else{
            fil_veina_start=fil-1;
            fil_veina_end=fil+1;
        }

        Boolean[] possibilitatsCorrectes = new Boolean[4]; //4= 4costats possibles
        for (Boolean bool:possibilitatsCorrectes){
            bool=null;
            //Fotem la taula a null...
        }
        int contador =0; //Contador de costats nulls
        for (phil= fil_veina_start;phil<=fil_veina_end;phil++){
            for (collins=col_veina_start;collins<=col_veina_end;collins++){
                if (fil==phil ||col==collins){
                    Tarja tarja = mTauler[phil][collins];
                    if(tarja == null){ //Zona buida no fem res
                        contador++;
                        continue;
                    }
                    if(phil == fil && collins == col){
                        //Si entrem aqui es perque aquesta posicio ja esta ocupada ...
                        return false;
                    }
                    if(phil < fil){
                        //Posició superior
                        if(lateralsActuals[0] == tarja.getLaterals()[2]){
                            possibilitatsCorrectes[0] = true;
                        }else{
                            possibilitatsCorrectes[0] = false;
                        }
                    }
                    else if(phil > fil){
                        //posicio inferior
                        if(lateralsActuals[2] == tarja.getLaterals()[0]){
                            possibilitatsCorrectes[2] = true;
                        }else{
                            possibilitatsCorrectes[2] = false;
                        }
                    }
                    else if(collins < col){
                        //posicio esquerra
                        if(lateralsActuals[3] == tarja.getLaterals()[1]){
                            possibilitatsCorrectes[3] = true;
                        }else{
                            possibilitatsCorrectes[3] = false;
                        }
                    }
                    else{
                        //posicio dreta
                        if(lateralsActuals[1] == tarja.getLaterals()[3]){
                            possibilitatsCorrectes[1] = true;
                        }else{
                            possibilitatsCorrectes[1] = false;
                        }
                    }
                }
            }
        }
        boolean podemColocar =false;
        for (Boolean bool:possibilitatsCorrectes){
            if (bool==null){
                continue;
            }
            if (!bool){
                return false;
            }
            if (bool){
                podemColocar=true;
            }
        }
        if (podemColocar){
            return true;
        }else{
            return false;
        }
    }
}
