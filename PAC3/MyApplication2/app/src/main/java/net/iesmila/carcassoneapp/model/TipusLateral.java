package net.iesmila.carcassoneapp.model;

/**
 * Created by BERNAT on 13/04/2016.
 */
public enum TipusLateral {
    TERRA,
    CAMI,
    CASTELL
}
